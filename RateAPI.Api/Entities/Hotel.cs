using MongoDB.Bson.Serialization.Attributes;

namespace RateAPI.Api.Entities
{
    /// <summary>
    /// Model class for Hotel
    /// As the purpose of the test is a rate limiter, I decided to create only class to store datas
    /// </summary>
    public class Hotel
    {
        #region Properties

        /// <summary>
        /// Store the city name
        /// </summary>
        /// <value>City Name</value>
        public string CityName { get; set; }

        [BsonId]
        /// <summary>
        /// Store the hotel ID. It's the ID in the database
        /// </summary>
        /// <value>Hotel Id</value>
        public int HotelId { get; set; }

        /// <summary>
        /// Store the price value
        /// </summary>
        /// <value>Price</value>
        public int Price { get; set; }

        /// <summary>
        /// Store the room name
        /// </summary>
        /// <value>Room name</value>
        public string RoomName { get; set; }

        #endregion
    }
}