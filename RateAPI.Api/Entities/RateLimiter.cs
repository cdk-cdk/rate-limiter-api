using System;
using System.Threading.Channels;
using System.Timers;
using RateAPI.Api.Utilities;

namespace RateAPI.Api.Entities
{
    /// <summary>
    /// Class representing a rate limiter
    /// </summary>
    public class RateLimiter
    {
        #region Properties

        /// <summary>
        /// Gets or sets the bucket used to implement token bucket algorithm
        /// </summary>
        /// <value>Channel with a capacity equals to Threshold</value>
        public Channel<bool> Bucket
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the downtime period
        /// </summary>
        /// <value>Time in seconds, default 5</value>
        public int DowntimePeriod
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the rate limiter period
        /// </summary>
        /// <value>Time in seconds, default 10</value>
        public int Period
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the endpoint's availability
        /// </summary>
        /// <value>TRUE if unavailable, FALSE if available</value>
        public bool IsLock
        {
            get
            {
                return (this._isLock);
            }
            set
            {
                if (this._isLock != value)
                {
                    this._isLock = value;
                    this._timerConsumeTokens.Stop();
                    this._timerUnlockEndpoint.Start();
                }
            }
        }

        /// <summary>
        /// Gets or sets the threshold of requests within Duration period
        /// </summary>
        /// <value>Number of requests, default 50</value>
        public int Threshold
        {
            get;
            set;
        }

        #endregion

        #region Instance variables

        private bool _isLock;

        private Timer _timerConsumeTokens;
        private Timer _timerUnlockEndpoint;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="RateLimiter"/> class.
        /// </summary>
        /// <param name="downtimePeriod">Period (in seconds) during which the endpoint is unavailable</param>
        /// <param name="period">Period in seconds</param>
        /// <param name="threshold">Number of request which can be accepted in the period</param>
        public RateLimiter(int threshold = Constants.DefaultThreshold, int period = Constants.DefaultPeriod, int downtimePeriod = Constants.DefaultDowntimePeriod)
        {
            this.DowntimePeriod = downtimePeriod;
            this.Period = period;
            this.Threshold = threshold;
            this.Bucket = Channel.CreateBounded<bool>(new BoundedChannelOptions(threshold)
            {
                FullMode = BoundedChannelFullMode.Wait
            });

            this._timerConsumeTokens = new Timer();
            this._timerConsumeTokens.Interval = this.Period * 1000;
            this._timerConsumeTokens.Elapsed += this.OnTimerConsumeTokensTick;
            this._timerConsumeTokens.AutoReset = true;
            this._timerConsumeTokens.Enabled = true;

            this._timerUnlockEndpoint = new Timer();
            this._timerUnlockEndpoint.Interval = this.DowntimePeriod * 1000;
            this._timerUnlockEndpoint.Elapsed += this.OnTimerUnlockEndpointTick;
            this._timerUnlockEndpoint.Enabled = false;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Consumes tokens from the channel
        /// </summary>
        private void ConsumesTokens()
        {
            for (int i = 0; i < this.Threshold; i++)
            {
                if (!this.Bucket.Reader.TryRead(out bool result))
                    break;
            }
        }

        /// <summary>
        /// Occurs on the timer tick and consumes tokens from the channel
        /// </summary>
        /// <param name="sender">Object that raised the event</param>
        /// <param name="e">Event data</param>
        private void OnTimerConsumeTokensTick(object sender, EventArgs e)
        {
            this.ConsumesTokens();
        }

        /// <summary>
        /// Occurs on the timer tick and restart the consumer process
        /// </summary>
        /// <param name="sender">Object that raised the event</param>
        /// <param name="e">Event data</param>
        private void OnTimerUnlockEndpointTick(object sender, EventArgs e)
        {
            this._timerUnlockEndpoint.Stop();
            this._isLock = false;
            this.ConsumesTokens();
            this._timerConsumeTokens.Start();
        }

        #endregion
    }

}