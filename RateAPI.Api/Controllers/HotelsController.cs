using RateAPI.Api.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using RateAPI.Api.Entities;
using RateAPI.Api.Services;
using RateAPI.Api.Utilities;
using RateAPI.Api.Dtos;
using System.Threading.Tasks;
using System;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace RateAPI.Api.Controllers
{
    /// <summary>
    /// Hotels controller
    /// </summary>
    [ApiController]
    [Route("api/hotels")]
    public class HotelsController : ControllerBase
    {
        private readonly IHotelsRepository _repository;
        private readonly IRateLimiterService _service;
        private readonly ILogger<HotelsController> logger;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="repository">Hotels repository</param>
        /// <param name="service">Rate limiter service</param>
        public HotelsController(IHotelsRepository repository, IRateLimiterService service, ILogger<HotelsController> logger)
        {
            this._repository = repository;
            this.logger = logger;
            this._service = service;
            this._service.AddRateLimiter(Constants.GetCity);
            this._service.AddRateLimiter(Constants.GetRoom);
        }

        [HttpPost]
        public async Task<ActionResult<HotelDto>> CreateHotelAsync(CreateHotelDto hotelDto)
        {
            Hotel hotel = new()
            {
                CityName = hotelDto.CityName,
                HotelId = hotelDto.HotelId,
                Price = hotelDto.Price,
                RoomName = hotelDto.RoomName
            };

            await _repository.CreateHotelAsync(hotel);

            return CreatedAtAction(nameof(GetHotelAsync), new { hotelId = hotel.HotelId }, hotel.AsDto());
        }

        /// <summary>
        /// Delete an element
        /// </summary>
        /// <param name="id">ID of the element we want to delete</param>
        /// <returns>Success(204) or NotFound(404)</returns>
        [HttpDelete("{hotelId}")]
        public async Task<ActionResult> DeleteItemAsync(int hotelId)
        {
            var existingItem = _repository.GetHotelAsync(hotelId);

            if (existingItem is null)
            {
                return NotFound();
            }

            await _repository.DeleteHotelAsync(hotelId);

            return NoContent();
        }

        /// <summary>
        /// Gets an element in the database
        /// </summary>
        /// <param name="hotelId">ID</param>
        /// <returns>The hotel or NotFound(404)</returns>
        [HttpGet("{hotelId}")]
        public async Task<ActionResult<HotelDto>> GetHotelAsync(int hotelId)
        {
            var hotel =  await _repository.GetHotelAsync(hotelId);

            if (hotel is null)
                return NotFound();

            return hotel.AsDto();
        }

        /// <summary>
        /// Gets all hotels based in one specific city
        /// /api/hotels/city?cityname=Bangkok
        /// /api/hotels/city?cityname=Bangkok&sort=ASC
        /// </summary>
        /// <param name="cityName">City name, required</param>
        /// <param name="sort">Sort filter, optional</param>
        /// <returns>List of hotels</returns>
        [HttpGet("city")]
        public async Task<IEnumerable<HotelDto>> GetHotelsByCityAsync(string cityName, string sort = null)
        {
            if (!this._service.ConnexionSuccess(Constants.GetCity))
            {
                throw new ArgumentException("We can't provide this service.");
            }

            var hotels =  (await _repository.GetHotelsByCityAsync(cityName, sort)).Select(hotel => hotel.AsDto());

            string date = DateTime.UtcNow.ToString("hh:mm:ss");
            logger.LogInformation($"{date}: Retrieved {hotels.Count()} items");
            return (hotels);
        }

        /// <summary>
        /// Gets all hotels having this type of room
        /// /localhost:5000/api/hotels/room?roomname=Deluxe
        /// /localhost:5000/api/hotels/room?roomname=Deluxe&sort=ASC
        /// </summary>
        /// <param name="roomName">Type of room, required</param>
        /// <param name="sort">Sort filter, optional</param>
        /// <returns>List of hotels</returns>
        [HttpGet("room")]
        public async Task<IEnumerable<HotelDto>> GetHotelsByRoomAsync(string roomName, string sort = null)
        {
            if (!this._service.ConnexionSuccess(Constants.GetRoom))
            {
                throw new ArgumentException("We can't provide this service.");
            }

            var hotels = (await _repository.GetHotelsByRoomAsync(roomName, sort))
                            .Select(hotel => hotel.AsDto());
            return (hotels);
        }
    }
}