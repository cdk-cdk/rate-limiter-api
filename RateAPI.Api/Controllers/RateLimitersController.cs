using Microsoft.AspNetCore.Mvc;
using RateAPI.Api.Dtos;
using RateAPI.Api.Entities;
using RateAPI.Api.Repositories;
using RateAPI.Api.Services;
using RateAPI.Api.Utilities;

namespace RateAPI.Api.Controllers
{
    /// <summary>
    /// Rate Limiters controller
    /// </summary>
    [ApiController]
    [Route("api/ratelimiters")]
    public class RateLimitersController : ControllerBase
    {
        private readonly IRateLimitersRepository _repository;
        private readonly IRateLimiterService _service;

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="repository">Hotels repository</param>
        /// <param name="service">Rate limiter service</param>
        public RateLimitersController(IRateLimitersRepository repository, IRateLimiterService service)
        {
            this._repository = repository;
            this._service = service;
            this._service.AddRateLimiter(Constants.PutRateLimiter);
        }

        /// <summary>
        /// Update rate limiter of a specific endpoint
        /// /api/ratelimiters/{endpoint}
        /// </summary>
        /// <param name="endpoint">Endpoint we want to modify</param>
        /// <param name="rateLimiterDto">New configuration for the rate limiter</param>
        /// <returns></returns>
        [HttpPut("{endpoint}")]
        public ActionResult<UpdateRateLimiterDto> UpdateRateLimiter(string endpoint, UpdateRateLimiterDto rateLimiterDto)
        {
            if (!this._service.ConnexionSuccess(Constants.PutRateLimiter))
                return BadRequest();

            if (this._repository.GetRateLimiter(endpoint) == null)
                return NotFound();

            RateLimiter result = this._service.UpdateRateLimiter(endpoint, rateLimiterDto.Threshold, rateLimiterDto.Period, rateLimiterDto.DowntimePeriod);

            return (NoContent());
        }
    }
}