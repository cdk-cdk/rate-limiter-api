using System.Collections.Concurrent;
using RateAPI.Api.Entities;

namespace RateAPI.Api.Services
{
    public interface IRateLimiterService
    {
        static ConcurrentDictionary<string, RateLimiter> Connexions { get; }
        void AddRateLimiter(string endpoint);
        bool ConnexionSuccess(string endpoint);
        RateLimiter UpdateRateLimiter(string endpoint, int threshold, int period, int downtimePeriod);
    }
}