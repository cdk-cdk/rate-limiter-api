using System;
using System.Collections.Concurrent;
using RateAPI.Api.Entities;
using RateAPI.Api.Utilities;

namespace RateAPI.Api.Services
{
    /// <summary>
    /// Rate limiter service
    /// Acting as producer / consumer
    /// </summary>
    public class RateLimiterService : IRateLimiterService
    {
        #region Properties

        /// <summary>
        /// Gets the dictionnary storing channels by endpoint
        /// </summary>
        /// <value>Channels endpoints</value>
        public static ConcurrentDictionary<string, RateLimiter> Connexions = new ConcurrentDictionary<string, RateLimiter>();

        #endregion

        #region Public methods

        /// <summary>
        /// Creates an entry to the Connexions dictionnary
        /// Handles endpoints where no configuration was specified
        /// </summary>
        /// <param name="endpoint">Endpoint name</param>
        public void AddRateLimiter(string endpoint)
        {
            if (!RateLimiterConfiguration.RateLimiters.ContainsKey(endpoint))
                RateLimiterConfiguration.RateLimiters.Add(endpoint, new RateLimiter());

            Connexions.TryAdd(endpoint, RateLimiterConfiguration.RateLimiters[endpoint]);
        }

        /// <summary>
        /// Checks if the request can be accepted
        /// Procudes a token in the channel
        /// </summary>
        /// <param name="endpoint">Endpoint name</param>
        /// <returns>TRUE if the request can be accepted, otherwise FALSE.</returns>
        public bool ConnexionSuccess(string endpoint)
        {
            RateLimiter rateLimiter = Connexions[endpoint];
            bool success = rateLimiter.Bucket.Writer.TryWrite(true);
            
            if(!success)
                rateLimiter.IsLock = true;
            
            return (success);
        }

        /// <summary>
        /// Update rate limiter for a specific endpoint
        /// </summary>
        /// <param name="endpoint">Endpoint name</param>
        /// <param name="threshold">Number of request which can be accepted in the period</param>
        /// <param name="period">Period in seconds</param>
        /// <param name="downtimePeriod">Period (in seconds) during which the endpoint is unavailable</param>
        /// <returns>Updated rate limiter</returns>
        public RateLimiter UpdateRateLimiter(string endpoint, int threshold, int period, int downtimePeriod)
        {
            if (RateLimiterConfiguration.RateLimiters.ContainsKey(endpoint))
                Connexions.AddOrUpdate(endpoint, new RateLimiter(threshold, period, downtimePeriod), (key, oldValue) => new RateLimiter(threshold, period, downtimePeriod));

            Connexions.TryGetValue(endpoint, out RateLimiter result);

            return (result);
        }

        #endregion
    }
}