using System;
using System.ComponentModel.DataAnnotations;

namespace RateAPI.Api.Dtos
{
    public record HotelDto(string CityName, int HotelId, int Price, string RoomName);
    public record CreateHotelDto([Required] string CityName, [Required] int HotelId, [Required] int Price, [Required] string RoomName);
    public record UpdateRateLimiterDto([Required][Range(1, int.MaxValue)] int DowntimePeriod,
                                        [Required][Range(1, int.MaxValue)] int Period,
                                        [Required][Range(1, int.MaxValue)] int Threshold);
}   