using RateAPI.Api.Dtos;
using RateAPI.Api.Entities;

namespace RateAPI.Api
{
    public static class Extensions
    {
        public static HotelDto AsDto(this Hotel hotel)
        {
            return new HotelDto(hotel.CityName, hotel.HotelId, hotel.Price, hotel.RoomName);
        }
    }
}