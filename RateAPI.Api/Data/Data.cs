using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RateAPI.Api.Entities;

namespace RateAPI.Api.Data
{
    /// <summary>
    /// Data extractor from CSV file
    /// Old class used as input for Agoda assignment
    /// </summary>
    public static class DataRetriever
    {
        #region Public methods

        /// <summary>
        /// Import datas
        /// </summary>
        /// <returns>List of hotels</returns>
        public static List<Hotel> GetHotelsFromCSV()
        {
            try
            {
                List<Hotel> datas = new List<Hotel>();

                using (StreamReader reader = new StreamReader("Data/hoteldb.csv"))
                {
                    // We read one line

                    bool firstLine = true;

                    while (reader.Peek() > -1)
                    {
                        string line = reader.ReadLine();

                        if (firstLine)
                        {
                            firstLine = false;
                            continue;
                        }

                        // We extract one line

                        Hotel lineData = DataRetriever.Parse(line);

                        if (lineData != null)
                            datas.Add(lineData);
                    }
                }

                return (datas);
            }
            catch (Exception)
            {
                return new List<Hotel>();
            }
        }

        /// <summary>
        /// Sort hotels by room price
        /// </summary>
        /// <param name="hotels">List of hotels</param>
        /// <param name="sort">ASC or DESC</param>
        /// <returns>Sortered list of hotels</returns>
        public static List<Hotel> GetHotelsByPrice(List<Hotel> hotels, string sort)
        {
            switch (sort.ToLower())
            {
                case "asc":
                    return hotels.OrderBy(h => h.Price).ToList();

                case "desc":
                    return hotels.OrderByDescending(h => h.Price).ToList();

                default:
                    return hotels;
            }
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Create and verify one import line
        /// </summary>
        /// <param name="datas">One line of datas</param>
        /// <returns>Parsed datas or null</returns>
        private static Hotel Parse(string datas)
        {
            Hotel result;

            if (string.IsNullOrWhiteSpace(datas))
                return (null);

            // Parsing

            string[] parsedValues = datas.Split(",");

            if (parsedValues.Length != 4)
                return (null);

            result = new Hotel();

            if (!int.TryParse(parsedValues[1], out int hotelId))
                return (null);

            if (!int.TryParse(parsedValues[3], out int price))
                return (null);

            result.CityName = parsedValues[0];
            result.RoomName = parsedValues[2];
            result.HotelId = hotelId;
            result.Price = price;

            return (result);
        }

        #endregion
    }
}