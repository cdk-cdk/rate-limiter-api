using RateAPI.Api.Entities;
using RateAPI.Api.Utilities;

namespace RateAPI.Api.Repositories
{
    /// <summary>
    /// RateLimiters repository
    /// </summary>
    public class RateLimitersRepository : IRateLimitersRepository
    {
        #region Constructor

        /// <summary>
        /// Gets the rate limiter matching the endpoint
        /// </summary>
        /// <param name="endpoint">Specific endpoint</param>
        /// <returns>Rate limiter</returns>
        public RateLimiter GetRateLimiter(string endpoint)
        {
            return (RateLimiterConfiguration.RateLimiters.ContainsKey(endpoint) ? RateLimiterConfiguration.RateLimiters[endpoint] : null);
        }

        #endregion
    }

}