using System.Collections.Generic;
using System.Threading.Tasks;
using RateAPI.Api.Entities;

namespace RateAPI.Api.Repositories
{
    /// <summary>
    /// Hotels repository interface
    /// </summary>
    public interface IHotelsRepository
    {
        /// <summary>
        /// Create a hotel in the database
        /// </summary>
        /// <param name="hotel">New hotel to insert</param>
        /// <returns>Task</returns>
        Task CreateHotelAsync(Hotel hotel);

        /// <summary>
        /// Delete an hotel in the database
        /// </summary>
        /// <param name="hotelId">HotelId of the hotel we want to delete</param>
        Task DeleteHotelAsync(int hotelId);

        /// <summary>
        /// Gets one hotel
        /// </summary>
        /// <param name="hotelId">Id of the hotel</param>
        /// <returns>One hotel matching hotel id</returns>
        Task<Hotel> GetHotelAsync(int hotelId);

        /// <summary>
        /// Gets all hotels
        /// </summary>
        /// <returns>List of hotels</returns>
        Task<IEnumerable<Hotel>> GetHotelsAsync();

        /// <summary>
        /// Gets all hotels based in one specific city
        /// </summary>
        /// <param name="cityName">City filter</param>
        /// <param name="sort">Price ordering, null by default</param>
        /// <returns>List of hotels</returns>
        Task<IEnumerable<Hotel>> GetHotelsByCityAsync(string cityName, string sort = null);

        /// <summary>
        /// Gets all hotels having this type of room
        /// </summary>
        /// <param name="roomName">Type of room</param>
        /// <param name="sort">Price ordering, null by default</param>
        /// <returns>List of hotels</returns>
        Task<IEnumerable<Hotel>> GetHotelsByRoomAsync(string roomName, string sort = null);
    }
}