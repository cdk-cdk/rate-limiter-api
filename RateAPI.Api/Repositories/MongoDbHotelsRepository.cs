using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using RateAPI.Api.Entities;

namespace RateAPI.Api.Repositories
{
    /// <summary>
    /// MongoDb repository for hostels
    /// </summary>
    public class MongoDbItemsRepository : IHotelsRepository
    {
        private const string databaseName = "rate";
        private const string collectionName = "hotels";
        private readonly IMongoCollection<Hotel> hotelsCollection;
        private readonly FilterDefinitionBuilder<Hotel> filterBuilder = Builders<Hotel>.Filter;

        #region Constructor

        /// <summary>
        /// Class onstructor
        /// </summary>
        /// <param name="mongoClient"></param>
        public MongoDbItemsRepository(IMongoClient mongoClient)
        {
            IMongoDatabase database = mongoClient.GetDatabase(databaseName);
            hotelsCollection = database.GetCollection<Hotel>(collectionName);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Insert a hotel in the database
        /// </summary>
        /// <param name="hotel">New hotel</param>
        public async Task CreateHotelAsync(Hotel hotel)
        {
            await hotelsCollection.InsertOneAsync(hotel);
        }

        /// <summary>
        /// Delete an hotel in the database
        /// </summary>
        /// <param name="hotelId">HotelId of the hotel we want to delete</param>
        public async Task DeleteHotelAsync(int hotelId)
        {
            var filter = filterBuilder.Eq(hotel => hotel.HotelId, hotelId);
            await hotelsCollection.DeleteOneAsync(filter);
        }

        /// <summary>
        /// Gets one hotel
        /// </summary>
        /// <param name="hotelId">Id of the hotel</param>
        /// <returns>One hotel matching hotel id</returns>
        public async Task<Hotel> GetHotelAsync(int hotelId)
        {
            var filter = filterBuilder.Eq(hotel => hotel.HotelId, hotelId);
            return await hotelsCollection.Find(filter).SingleOrDefaultAsync();
        }

        /// <summary>
        /// Gets all hotels
        /// </summary>
        /// <returns>List of hotels</returns>
        public async Task<IEnumerable<Hotel>> GetHotelsAsync()
        {
            return await hotelsCollection.Find(new BsonDocument()).ToListAsync();
        }

        /// <summary>
        /// Gets all hotels based in one specific city
        /// </summary>
        /// <param name="cityName">City filter</param>
        /// <param name="sort">Price ordering, null by default</param>
        /// <returns>List of hotels</returns>
        public async Task<IEnumerable<Hotel>> GetHotelsByCityAsync(string cityName, string sort = null)
        {
            var hotels = await hotelsCollection.Find(new BsonDocument()).ToListAsync();
            hotels = hotels.Where(item => item.CityName.ToLower() == cityName.ToLower()).ToList();

            if (sort is not null)
                hotels = sort.ToLower() == "asc" ? hotels.OrderBy(h => h.Price).ToList() : hotels.OrderByDescending(h => h.Price).ToList();

            return (hotels);
        }

        /// <summary>
        /// Gets all hotels having this type of room
        /// </summary>
        /// <param name="roomName">Type of room</param>
        /// <param name="sort">Price ordering, null by default</param>
        /// <returns>List of hotels</returns>
        public async Task<IEnumerable<Hotel>> GetHotelsByRoomAsync(string roomName, string sort = null)
        {
            var hotels = await hotelsCollection.Find(new BsonDocument()).ToListAsync();
            hotels = hotels.Where(item => item.RoomName == roomName).ToList();

            if (sort is not null)
                hotels = sort.ToLower() == "asc" ? hotels.OrderBy(h => h.Price).ToList() : hotels.OrderByDescending(h => h.Price).ToList();

            return (hotels);
        }

        #endregion
    }
}