using RateAPI.Api.Entities;

namespace RateAPI.Api.Repositories
{
    /// <summary>
    /// Rate limiters repository interface
    /// </summary>
    public interface IRateLimitersRepository
    {
        /// <summary>
        /// Gets the rate limiter matching the endpoint
        /// </summary>
        /// <param name="endpoint">Specific endpoint</param>
        /// <returns>Rate limiter</returns>
        RateLimiter GetRateLimiter(string endpoint);
    }
}