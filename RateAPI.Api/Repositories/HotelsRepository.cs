using System.Collections.Generic;
using RateAPI.Api.Entities;
using RateAPI.Api.Data;
using System.Threading.Tasks;

namespace RateAPI.Api.Repositories
{
    /// <summary>
    /// Hotels Repository
    /// </summary>
    public class HotelsRepository : IHotelsRepository
    {
        #region Public methods

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="hotelId">ID</param>
        public async Task CreateHotelAsync(Hotel hotel)
        {
            await Task.CompletedTask;
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="hotelId">ID</param>
        public async Task DeleteHotelAsync(int hotelId)
        {
            await Task.CompletedTask;
        }

        /// <summary>
        /// Gets one hotel
        /// </summary>
        /// <param name="hotelId">Id of the hotel</param>
        /// <returns>One hotel matching hotel id</returns>
        public async Task<Hotel> GetHotelAsync(int hotelId)
        {
            List<Hotel> hotels = DataRetriever.GetHotelsFromCSV();
            return (await Task.FromResult(hotels.Find(h => h.HotelId == hotelId)));
        }

        /// <summary>
        /// Gets all hotels
        /// </summary>
        /// <returns>List of hotels</returns>
        public async Task<IEnumerable<Hotel>> GetHotelsAsync()
        {
            return (await Task.FromResult(DataRetriever.GetHotelsFromCSV()));
        }

        /// <summary>
        /// Implementation of GetHotelsByCity
        /// </summary>
        /// <param name="cityName">City filter</param>
        /// <param name="sort">Sort filter</param> 
        /// <returns>List of hotels</returns>
        public async Task<IEnumerable<Hotel>> GetHotelsByCityAsync(string cityName, string sort = null)
        {
            List<Hotel> hotels = DataRetriever.GetHotelsFromCSV();

            // Filter by city name

            hotels = hotels.FindAll(h => h.CityName.ToLower() == cityName.ToLower());

            // Sort by room price

            if (sort != null)
                hotels = DataRetriever.GetHotelsByPrice(hotels, sort);

            return (await Task.FromResult(hotels));
        }

        /// <summary>
        /// Implementation of GetHotelsByRoom
        /// </summary>
        /// <param name="roomName">Type of room</param>
        /// <param name="sort">Sort filter</param> 
        /// <returns>List of hotels</returns>
        public async Task<IEnumerable<Hotel>> GetHotelsByRoomAsync(string roomName, string sort = null)
        {
            List<Hotel> hotels = DataRetriever.GetHotelsFromCSV();

            // Filter by room name

            hotels = hotels.FindAll(h => h.RoomName.ToLower() == roomName.ToLower());

            // Sort by room price

            if (sort != null)
                hotels = DataRetriever.GetHotelsByPrice(hotels, sort);

            return (await Task.FromResult(hotels));
        }

        #endregion
    }
}