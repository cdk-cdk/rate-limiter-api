using System.Collections.Generic;
using RateAPI.Api.Entities;

namespace RateAPI.Api.Utilities
{
    /// <summary>
    /// Class storing datas of endpoint rate limiters 
    /// </summary>
    public static class RateLimiterConfiguration
    {
        /// <summary>
        /// Gets rate limiters by endpoint
        /// </summary>
        /// <typeparam name="string">Endpoint name, ex: getcity</typeparam>
        /// <typeparam name="RateLimiter">Rate limiter of the endpoint</typeparam>
        /// <returns>Rate limiter by endpoint</returns>
        public static Dictionary<string, RateLimiter> RateLimiters = new Dictionary<string, RateLimiter>()
        {
            {Constants.Default, new RateLimiter()},
            {Constants.GetCity, new RateLimiter(Constants.GetCityThreshold, Constants.GetCityPeriod, Constants.GetCityDowntimePeriod)},
            {Constants.GetRoom, new RateLimiter(Constants.GetRoomThreshold, Constants.GetRoomPeriod, Constants.GetRoomDowntimePeriod)}
        };

        /// <summary>
        /// Default Rate limiter configuration
        /// </summary>
        /// <value>Configuration</value>
        public static RateLimiter Default 
        {
            get
            {
                return (RateLimiters[Constants.Default]);
            }
        }

        /// <summary>
        /// Rate limiter configuration for endpoint (GET) hotels/city
        /// </summary>
        /// <value>Configuration</value>
        public static RateLimiter GetCity 
        {
            get
            {
                return (RateLimiters[Constants.GetCity]);
            }
        }

        /// <summary>
        /// Rate limiter configuration for endpoint (GET) hotels/room 
        /// </summary>
        /// <value>Configuration</value>
        public static RateLimiter GetRoom 
        {
            get
            {
                return (RateLimiters[Constants.GetRoom]);
            }
        }
    }
}