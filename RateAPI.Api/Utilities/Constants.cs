namespace RateAPI.Api.Utilities
{
    /// <summary>
    /// Stores datas such as endpoints names
    /// </summary>
    public static class Constants
    {
        public const string Default = "default";
        public const string GetCity = "getcity";
        public const string GetRoom = "getroom";
        public const string PutRateLimiter = "putratelimiter";

        public const int DefaultThreshold = 50;
        public const int DefaultPeriod = 10;
        public const int DefaultDowntimePeriod = 5;

        public const int GetCityThreshold = 5;
        public const int GetCityPeriod = 20;
        public const int GetCityDowntimePeriod = 5;

        public const int GetRoomThreshold = 100;
        public const int GetRoomPeriod = 10;
        public const int GetRoomDowntimePeriod = 5;
    }
}