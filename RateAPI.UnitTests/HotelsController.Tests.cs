using System;
using Xunit;
using Moq;
using RateAPI.Api.Repositories;
using RateAPI.Api.Services;
using RateAPI.Api.Controllers;
using System.Collections.Generic;
using RateAPI.Api.Entities;
using FluentAssertions;
using RateAPI.Api.Dtos;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace RateAPI.UnitTests
{
    /// <summary>
    /// Test class for HotelsController
    /// </summary>
    public class HotelsControllerTests
    {
        private readonly Mock<IHotelsRepository> _repositoryStub = new();
        private readonly Mock<IRateLimiterService> _serviceStub = new();
        private readonly Mock<ILogger<HotelsController>> _loggerStub = new();

        #region Public methods

        /// <summary>
        /// Test for GetHotelsByCity
        /// We ensure that BadRequest is sent when we can't accept the request
        /// </summary>
        [Fact]
        public async Task GetHotelsByCity_WithRequestsLimitReached_ThrowsErrorAsync()
        {
            // Arrange 

            var expectedHotels = this.CreateMockHotelsList();

            this._repositoryStub.Setup(repo => repo.GetHotelsByCityAsync(It.IsAny<string>(), "ASC"))
                .ReturnsAsync(expectedHotels);

            this._serviceStub.Setup(repo => repo.ConnexionSuccess(It.IsAny<string>()))
                .Returns(false);

            var controller = new HotelsController(this._repositoryStub.Object, this._serviceStub.Object, this._loggerStub.Object);

            // Act
            // Assert

            ArgumentException exception = await Assert.ThrowsAsync<ArgumentException>(() => controller.GetHotelsByCityAsync("Bangkok", "ASC"));
            Assert.Equal("We can't provide this service.", exception.Message);
        }

        /// <summary>
        /// Test for GetHotelsByCity
        /// We ensure that a list of hotels is sent
        /// </summary>
        [Fact]
        public async void GetHotelsByCity_WithRequestAvailable_ReturnsHotelsList()
        {
            // Arrange 

            var expectedHotels = this.CreateMockHotelsList();

            this._repositoryStub.Setup(repo => repo.GetHotelsByCityAsync(It.IsAny<string>(), "ASC")).ReturnsAsync(expectedHotels);

            this._serviceStub.Setup(repo => repo.ConnexionSuccess(It.IsAny<string>())).Returns(true);

            var controller = new HotelsController(this._repositoryStub.Object, this._serviceStub.Object, this._loggerStub.Object);

            // Act

            IEnumerable<HotelDto> result = await controller.GetHotelsByCityAsync("Bangkok", "ASC");

            // Assert

            result.Should().BeEquivalentTo(expectedHotels);
        }

        /// <summary>
        /// Test for GetHotelsByRoom
        /// We ensure that BadRequest is sent when we can't accept the request
        /// </summary>
        [Fact]
        public async Task GetHotelsByRoom_WithRequestsLimitReached_ReturnsBadRequest()
        {
            // Arrange 
            
            var expectedHotels = this.CreateMockHotelsList();

            this._repositoryStub.Setup(repo => repo.GetHotelsByRoomAsync(It.IsAny<string>(), "ASC"))
                .ReturnsAsync(expectedHotels);

            this._serviceStub.Setup(repo => repo.ConnexionSuccess(It.IsAny<string>()))
                .Returns(false);

            var controller = new HotelsController(this._repositoryStub.Object, this._serviceStub.Object, this._loggerStub.Object);

            // Act
            // Assert

            ArgumentException exception = await Assert.ThrowsAsync<ArgumentException>(() => controller.GetHotelsByRoomAsync("Bangkok", "ASC"));
            Assert.Equal("We can't provide this service.", exception.Message);
        }
            
        #endregion

        #region Private methods

        /// <summary>
        /// Create a mock list of hotels
        /// </summary>
        /// <param name="count">Number of hotels</param>
        /// <returns>A hotels list of size "count"</returns>
        private List<Hotel> CreateMockHotelsList(int count = 10)
        {
            List<Hotel> result = new List<Hotel>();

            count = count < 1 ? 10 : count;

            for(int i = 1; i < count+1 ; i++)
                result.Add(new Hotel(){CityName = "Bangkok", HotelId = i, RoomName = "Deluxe", Price = i*1000});

            return (result);
        }
            
        #endregion
    }
}
