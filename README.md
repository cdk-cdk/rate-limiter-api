# API with a rate limiting implementation

Project is based on C# .NET Core 5.0. Integration testing with [xUnit](https://xunit.net/). I use Docker and MongoDB.

To implement the rate limiter behaviour, I implemented the token bucket algorithm using [Channel](https://docs.microsoft.com/en-us/dotnet/api/system.threading.channels.channel?view=net-5.0) (bounded channel). There is a Producer/Consomer pattern which avoids using a key-value store.

I set the channel capacity equals to the maximum of requests an endpoint can handle. When a request is received, a token is added to the channel. When the channel limit is reached, no token can be added to the channel. It's this behaviour of bounded channels which informs us on when to lock the endpoint. 

When the channel limit is reached, I start a [Timer](https://docs.microsoft.com/en-us/dotnet/api/system.timers.timer?view=net-5.0). Meanwhile, no read of the channel can occur. So the channel stays full and all incoming requests are rejected.
I read the channel to empty the channel. This event occurs every X seconds as specified by the rate limiter configuration. 

I also thought about using [MemoryCache](https://docs.microsoft.com/en-us/dotnet/api/system.runtime.caching.memorycache?view=dotnet-plat-ext-5.0). I chose Channel over MemoryCache for two reasons:
1. Neat management of tokens consuming, with Channel, I decide when tokens will be released
2. Due to many requests incoming at the same time, MemoryCache might accept more requests than specified. Whereas, Channel will never accept more than specified.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

.NET Core 5.0 is needed, download it using the following link [.NET 5.0](https://dotnet.microsoft.com/download).
For testing purposes, two packages are necessary: [FluentAssertions](https://www.nuget.org/packages/FluentAssertions/) and [moq](https://www.nuget.org/packages/moq/).
For MongoDB, [MongoDB.Driver](https://www.nuget.org/packages/mongodb.driver).
For health checks : [AspNetCore.HealthChecks.Mongodb](https://github.com/Xabaril/AspNetCore.Diagnostics.HealthChecks).

### Installing

Clone the repository using

```bash
git clone https://gitlab.com/cdk-cdk/rate-limiter-api.git
```

Open a terminal and type
```powershell
# For the API
cd .\RateAPI.Api\
dotnet restore "RateAPI.Api/RateAPI.Api.csproj"
dotnet run

# MongoDB container
docker run -d --rm --name mongo -p 27017:27017 -v mongodbdata:/data/db -e MONGO_INITDB_ROOT_USERNAME=mongoadmin -e MONGO_INITDB_ROOT_PASSWORD=Pass#word1
```

Also, you can run the solution with your favorite IDE, Visual Studio Code was used for this project.

### Verify the deployment

Verify the deployment by navigating to the server address in any browser.
```bash
http://localhost:5000/swagger/index.html
```

Then, try the health checks endpoints (availables in postman files): 
```bash
http://{{url}}/health/ready
http://{{url}}/health/live
# with {{url}} equals to localhost:5000 or localhost:8080
```

## Try it with Docker

You can test my API from docker, run the following commands

```powershell
# Create the network to allow containers to communicate together
docker network create apinetwork
# MongoDB container
docker run -d --rm --name mongo -p 27017:27017 -v mongodbdata:/data/db -e MONGO_INITDB_ROOT_USERNAME=mongoadmin -e MONGO_INITDB_ROOT_PASSWORD=Pass#word1 --network=apinetwork mongo
# The API container
docker run -d --rm -p 8080:80 -e MongoDbSettings:Host=mongo -e MongoDbSettings:Password=Pass#word1 --network=apinetwork cdkcdk/rateapi:v1.0
```

## Endpoints
Please review the [swagger](http://localhost:5000/swagger/index.html).

**Get all hotels matching a City (cityname: required, sort: optional)**
```bash
(GET) http://localhost:5000/api/hotels/city?cityname={{cityname}}&sort={{sort}}
# examples:
http://localhost:5000/api/hotels/city?cityname=Bangkok
http://localhost:5000/api/hotels/city?cityname=Bangkok&sort=ASC
```

**Get all hotels matching a room type (roomname: required, sort: optional)**
```bash
(GET) http://localhost:5000/api/hotels/room?roomname={{roomname}}&sort={{sort}}
#examples:
http://localhost:5000/api/hotels/room?roomname=Deluxe
http://localhost:5000/api/hotels/room?roomname=Sweet Suite&sort=DESC
```

**Update rate limiters value for an endpoint.** When a rate limiter is updated, we dispose the previous configuration.

```bash
(PUT) http://localhost:5000/api/ratelimiters/{{endpoint}}
examples: 
http://localhost:5000/api/ratelimiters/getcity
http://localhost:5000/api/ratelimiters/getroom
http://localhost:5000/api/ratelimiters/putratelimiter
```
With the following body:
```json
{
    "downtimePeriod": {{downtimePeriod}},
    "period": {{period}},
    "threshold": {{threshold}}
}
```
```json
{
    "downtimePeriod": 8, 
    "period": 20, 
    "threshold": 40
}
```

**Create a hotel**
```bash
(POST) https://localhost:5001/api/hotels
examples:
{
  "cityName": "Berlin",
  "hotelId": 3,
  "price": 1900,
  "roomName": "Sweet Suite"
}
```


**Health checks: live and ready**
```bash
(GET) http://localhost:5000/health/ready
(GET) http://localhost:5000/health/live
```

### Testing with Postman

I provide a Postman collection and environment in the folder "rate-limiter-api\Postman". Here is a [Postman tutorial](https://learning.postman.com/docs/sending-requests/managing-environments/) on managing environments.

To demonstrate the rate limiting, we can use the [Postman Runner](https://learning.postman.com/docs/running-collections/intro-to-collection-runs/). Here is a screenshot of the runner as used to test the API.
![postman runner screenshot](Postman/RunnerPostmanV8.PNG).

### Configuration
All endpoint names values are defined in rate-limiter-api\Utilities\Constants.cs.
Rate limiters default values are defined in rate-limiter-api\Utilities\Constants.cs too. Modify values in this file or through api/ratelimiters/{endpoint} endpoint.

## Testing

At the root folder, open a terminal and type

```powershell
cd .\RateAPI.UnitTests\
dotnet restore "RateAPI.Api/RateAPI.UnitTests.csproj"
dotnet test
```

The result shall be: 

```powershell
Passed!  - Failed:     0, Passed:     3, Skipped:     0, Total:     3, Duration: 74 ms - RateAPI.UnitTests.dll (net5.0)
```